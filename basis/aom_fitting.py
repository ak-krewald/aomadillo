"""
AOM Fitting Module
Heart of the aomadillo project.

This is one of the main modules of the whole AOM fitting package, it provides
all functions needed to construct the ligand field matrix, and some more for
reading geometries and ORCA output files.

Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""

import functools
import math

from numpy import sin, cos
from scipy.special import binom
from scipy.spatial.transform import Rotation

import itertools as it
import numpy as np
import os


hartree_to = {
    'eV' : 27.211386245,
    'kJ/mol' : 2625.499639,
    'cm-1' : 219474.631363
    }

################################ LICENSE CHECK #################################
def check_license():
    path = os.path.dirname(__file__)
    with open(f"{path}/../README", 'r') as f:
        if f.readlines()[0].split("=")[1].strip().lower() != "true":
            raise SystemExit("Please read the README first and confirm that you accept the license.")
################################################################################

############################# DEBUGGING DECORATORS #############################
def printParams(func):
    """
    Debugging decorator which prints the parameters of a function.
    """
    @functools.wraps(func)
    def wrapperPrintParams(*args, **kwargs):
        print(*args, end='\n')
        #print(**kwargs, end='\n')
        return func(*args, **kwargs)
    return wrapperPrintParams

def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug
################################################################################


def readLigandFieldOneElectronMatrix(file: str, mode: str) -> np.array:
    """
    Reads in ORCA CASSCF output file and returns the ligand field one-electron matrix VLFT = <d|^V_LFT|d'>.
    @param file: String. ORCA CASSCF output file
    @param mode: String. 'NEVPT2' or 'CASSCF'. Definition of mode in f'AILFT MATRIX ELEMENTS ({mode})'
    @return: 2D numpy.array. Contains the one electron matrix elements in Eh: [[line 1],[line 2],...]
    """
    assert mode in ['NEVPT2', 'CASSCF']
    with open(file, 'r') as f:
        writing = False
        remaining_lines = 0
        matrix = []
        for line in f:
            if writing:
                matrix.append(line.split()[1:])
                remaining_lines -= 1
            elif f'AILFT MATRIX ELEMENTS ({mode})' in line and not writing:
                writing = True
                remaining_lines = 9
            if writing and remaining_lines == 0:
                break
    if len(matrix) == 0:
        raise ValueError(f'The file {file} did not contain the requested one electron matrix. Mode was {mode}.')
    detection_header = matrix[3]
    matrix = matrix[4:]
    for i in range(5):
        matrix[i] = list(map(float, matrix[i]))
    if len(detection_header) == 5: #ORCA5 detection
        orca5_dic = {0 : 4,
                     1 : 2,
                     2 : 0,
                     3 : 1,
                     4 : 3}
        reordered_mat = np.zeros((5,5))
        for i,j in it.product(range(5), repeat=2):
            reordered_mat[i][j] = matrix[orca5_dic[i]][orca5_dic[j]]
        matrix = reordered_mat
    else:
        matrix = np.array(matrix)
    return matrix
#EOFunction

def xyzToPol(x: float, y: float, z: float, deg: bool=False) -> [float, float]:
    """
    Transforms a set of cartesian coordinates to polar coordinates.
    @param x,y,z: Cartesian coordinates.
    @return: theta, phi polar coordinates. No r is returned!
    """
    r = math.sqrt(x**2 + y**2 + z**2)
    if r != 0:
        theta = np.arccos(z/r)
    else:
        theta = 0
    phi = math.atan2(y,x)
    if deg:
        return theta*57.2958, phi*57.2958
    else:
        return [theta, phi]
#EOFunction

def readGeometryFromCas(file: str, elements: bool=False) -> list[list[float]]:
    """
    Reads in a geometry from an ORCA CAS calculation file.
    @param file: String. Path to the output file to be read.
    @return: 2D list of cartesian coordinates: [[x1,y1,z1],[x2,y2,z2], ...]
    """
    raw_coordinates = []
    with open(file, 'r') as f:
        write = False
        for line in f:
            if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                write = True
            if 'CARTESIAN COORDINATES (A.U.)' in line:
                break
            if write:
                raw_coordinates.append(line)
        
    raw_coordinates = raw_coordinates[2:-2]
    out_coordinates = []
    
    for line in raw_coordinates:
        line = line.split()
        if elements:
            coords = [line[0], float(line[1]), float(line[2]), float(line[3])]
        else:
            coords = list(map(float,line[1:]))
        
        out_coordinates.append(coords)
    
    return out_coordinates
#EOFunction

def readGeometryFromXyz(file: str, elements: bool=False):
    """
    Read in xyz-file and store information in a list of [x,y,z] sublists.
    @param file: String. Path to the .xyz file.
    """
    with open(file, 'r') as f:
        full_file = f.readlines()
    
    if len(full_file[0].split()) == 1:
        full_file = full_file[2:]
    
    out_coordinates = []
    for line in full_file:
        line = line.split()
        
        if elements:
            out_coordinates.append([line[0], float(line[1]), float(line[2]), float(line[3])])
        else:
            out_coordinates.append([float(line[1]), float(line[2]), float(line[3])])
    
    return out_coordinates
#EOFunction

def readGeometry(file: str, elements=False) -> list:
    '''
    Reads in cartesian coordinates from ORCA CAS .out file or general .xyz file.
    @param file: path to the coordinate file. Must be a .xyz or ORCA CAS .out file. Length unit is Angstrom.
    '''
    if '.out' in file:
        xyz_file = readGeometryFromCas(file, elements=elements)
    elif '.xyz' in file:
        xyz_file = readGeometryFromXyz(file, elements=elements)
    else:
        raise RuntimeError('Unknown file ending. .xyz or ORCA CAS .out file required. ')
    return xyz_file

def getInteractionTypes(nods: bool, params_per_ligand: int) -> list[str]:
    """
    Builds a list of interaction types from ds-mixing and the number of parameters per ligand.
    @param nods: True if ds-mixing is turned off, otherwise false.
    @param params_per_ligand: Integer. Amount of AOM parameters per ligand.
    @return: List of strings. Contains a descriptive string for every interaction type.
    """
    if nods and params_per_ligand == 3:
        interaction_types = ['sigma', 'pix', 'piy']
    elif nods and params_per_ligand == 2:
        interaction_types = ['sigma', 'pi']
    elif not nods and params_per_ligand == 3:
        interaction_types = ['sigma', 'pi', 'ds']
    elif not nods and params_per_ligand == 4:
        interaction_types = ['sigma', 'pix', 'piy', 'ds']
    elif nods and params_per_ligand == 1:
        interaction_types = ['sigma']
    elif not nods and params_per_ligand == 2:
        interaction_types = ['sigma', 'ds']
    else:
        raise ValueError(f'Number of parameters {params_per_ligand} and ds-mixing-information (nods={nods}) do not lead to a known set of parameters.')
    
    return interaction_types

def buildGroupedCoefs(params_per_ligand: int, ligand_polars: list[list[float]], nods: bool=False) -> list:
    """
    Builds the list of matrix contributions to the ligand field matrix by calling
    dMatrixContribution and dsFunctions. There is also an identity
    matrix at first position (for E). Output is compatible to af.fifteenEquations.
    @param params_per_ligand: Integer. Number of parameters.
    @param ligand_polars: 2D List of polar coordinates. Second dimension
        contains exactly three floats.
    @param nods: Boolean. True if ds-mixing should not be considered.
    @return: Mixed list of ligand field matrix contributions. First dimension
        is the corresponding e parameter, in the sequence [E, es1, ep1, eds1, es2, ep2, eds2, ...]
        Second and third dimension are the matrix indices i and j.
        Coefs for ds-mixing are only a 1D sublist with D_i as elements.
    """
    out = []
    
    E = np.zeros((5,5))
    for i in range(5): E[i][i] = 1
    out.append(E)
    
    interaction_types = getInteractionTypes(nods, params_per_ligand)
    
    for polars in ligand_polars:
        for interaction_type in interaction_types:
            if interaction_type == 'pi': #special case that pix and piy are set to be equal
                coefficient_matrix = np.zeros((5,5))
                # polars has three elements (floats)
                coefficient_matrix += dMatrixContribution(*polars, 'pix')
                coefficient_matrix += dMatrixContribution(*polars, 'piy')
                out.append(coefficient_matrix)
            elif interaction_type == 'ds': #d-s mixing only contributes a vector
                ds_vector = np.zeros((5))
                ds_vector += dsFunctions(*polars)
                out.append(ds_vector)
            else: #if nothing special applies, just calculate the matrix for the given interaction type
                coefficient_matrix = np.zeros((5,5))
                coefficient_matrix += dMatrixContribution(*polars, interaction_type)
                out.append(coefficient_matrix)
    return out

def groupAtoms(polar_list: list[list[float]], atom_types: list, unique_ligands: int, type_dict: dict) -> list:
    """
    Packs a list of groups with coordinates in every group. Used to pack unique
    ligand coordinates into a compact, iterable variable.
    @param polar_list: 2D list. Contains polar coordinates of every ligand atom (without r).
    @param atom_types: 1D list of atom types with the same indexing as polar_list.
    @param unique_ligands: Integer. Amount of unique (distinguishable) ligands in the complex.
    @param type_dict: Dictionary with ligand type to index number {type1:0, type2:1, ...}
    @return: 3D list. First level sublists are lists of polar coordinates of the
        same atom type. So the first index is used to access a certain ligand type.
        Second sublist contains the pairs [theta, phi], so the second index is used to
        access a certain atom. The third index then accesses the polar coordinates of
        this atom.
    """
    grouped_polars = list(it.repeat(0, unique_ligands))
    for i in range(len(grouped_polars)):
        grouped_polars[i] = []
    for i, atom in enumerate(polar_list):
        grouped_polars[type_dict.get(atom_types[i])].append(atom)
    return grouped_polars

def buildBounds(ep: int, ds: int, unique_ligands: int) -> (list[int], list[int]):
    """
    Defines bounds for least_squares solver. First boundary is for E,
    next ones for es, epi, eds. eds requires special bounds, since it is
    always positive.
    @param ep: Integer. Number of pi parameters.
    @param ds: Integer. Tells whether there is an eds (1) or not (0).
    @param unique_ligands: Number of ligand atoms in the system.
    @return lower: List of Integers. Lower boundaries for the AOM parameters.
    @return upper: List of Integers. Upper boundaries for the AOM parameters.
    """
    upper = [200000] * (1 + ep + ds)
    lower = [-200000] * (1 + ep + ds)
    if ds == 1:
        upper[-1] = 10000
        lower[-1] = 0
    upper *= unique_ligands
    lower *= unique_ligands
    upper =  [0] + upper
    lower = [-10000000] + lower
    return lower,upper

def processPlotAtoms(expr: str) -> list[str]:
    """
    Processes plot atom group lists and returns it as list of strings.
    @return: List of strings
    """
    expr = expr.strip('[]')
    exprs = expr.split(',')
    return list(map(lambda x: x.strip(), exprs))

def processSoftGroupList(expr: str) -> list[list[int]]:
    """
    Processes grouping in the form of a list with atom groups belonging to indices:
    [1,2,1,3] would mean that the atoms at indices 0 and 2 form a group, index 1
    forms a group and index 3. Would return [[0,2],[1],[3]]
    @param expr: String as described above.
    @return: List of lists of integers as described above.
    """
    out = []
    exprs = processPlotAtoms(expr)
    for ex in set(exprs):
        out.append([])
        for j in range(len(exprs)):
            if ex == exprs[j]:
                out[-1].append(j)
    return out

############################ GEOMETRIC AUXILIARIES #############################
def planeAngles(vec_a, vec_b, vec_c, vec_d, vec_e, vec_f) -> float:
    """
    Returns the angle between two planes, i.e. their normal vectors.
    @param vecN: List of Float. First three points define the first plane,
        next three points define the second plane.
    @return: Float. Angle of two planes normals in radians.
    """
    vec_ba = vecFromAtoB(vec_b, vec_a)
    vec_bc = vecFromAtoB(vec_b, vec_c)
    norm_abc = np.cross(np.array(vec_ba), np.array(vec_bc))
    vec_ed = vecFromAtoB(vec_e, vec_d)
    vec_ef = vecFromAtoB(vec_e, vec_f)
    norm_def = np.cross(np.array(vec_ed), np.array(vec_ef))
    return subangle(norm_abc, norm_def)

def subangle(vec_a: list[float], vec_b: list[float], vec_n=None) -> float:
    """
    Returns the angle between two vectors.
    @param vec_n: List of Float, defining the two vectors.
    @return: Float. Angle of the two vectors in radians.
    """
    if vec_n is None:
        len_a = length(vec_a)
        len_b = length(vec_b)
        bruch = scalar(vec_a, vec_b)/(len_a * len_b)
        if bruch > 1 or bruch < -1:
            bruch = round(bruch, 8)
        out = np.arccos(bruch)
    else:
        normed_n = vec_n/length(vec_n)
        out = np.arctan2(scalar(np.cross(vec_b, vec_a), normed_n), scalar(vec_a, vec_b))
    return out

def vecFromAtoB(vec_a: list[float], vec_b: list[float]) -> list[float]:
    """
    Returns the connection vector from vec_a to vec_b.
    @return: List of Float.
    """
    return [b-a for a,b in zip(vec_a, vec_b)]

def scalar(vec_a: list[float], vec_b: list[float]) -> float:
    """
    Returns the scalar product of vec_a and vec_b.
    """
    return sum(a*b for a,b in zip(vec_a,vec_b))

def length(vec: list[float]) -> float:
    """
    Returns the length of a vector.
    """
    return np.sqrt(sum(a**2 for a in vec))
################################### GET PSI ###################################
def calcPsi(coords_ligand: list[float], coords_refatom: list[float]) -> float:
    """
    Calculate psi, that is the angle between the molecular plane (or whatever
    definition is used to distinguish between πx and πy) and the yz plane for
    a ligand that is located on the z-axis.
    @param coords_ligand: List of float. Cartesian coordinates of the ligand atom.
    @param coords_refatom: List of float. Cartesian coordinates of the reference.
    @return: Float. Described angle in radians.
    """
    polar_ligand = xyzToPol(*coords_ligand)
    rot = Rotation.from_euler('zy', [-polar_ligand[1], -polar_ligand[0]])
    rot_refatom = np.matmul(rot.as_matrix(), coords_refatom)
    norm_ligand = np.cross([0,0,1], rot_refatom)
    return subangle(norm_ligand, [0,1,0])

def calcPsis(coords_ligands: list[list[float]], coords_refatoms: list[list[float]]) -> list[float]:
    """
    Calculate psi as in calcPsi above for a list of ligand and reference coordinates.
    """
    assert len(coords_ligands) == len(coords_refatoms)
    psis = []
    for coords_ligand, coords_refatom in zip(coords_ligands, coords_refatoms):
        psis.append(calcPsi(coords_ligand, coords_refatom))
    return psis

########################## ANGULAR OVERLAP FUNCTIONS ##########################
#################################### SIGMA ####################################
def Fsdx2y2(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = math.sqrt(3)/4
    out *= np.cos(2*phi)
    out *= (1 - np.cos(2*theta))    
    return out

def Fsdz2(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = 0.25
    out += (3 * np.cos(2*theta))/4
    return out

def Fsdxy(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = math.sqrt(3)/4
    out *= np.sin(2*phi)
    out *= (1 - np.cos(2*theta))
    return out

def Fsdxz(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = math.sqrt(3)/2
    out *= np.cos(phi) * np.sin(2*theta)
    return out

def Fsdyz(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = math.sqrt(3)/2
    out *= np.sin(phi) * np.sin(2*theta)
    return out

#################################### PI Y #####################################
def Fpydx2y2(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = - (sin(2*phi) * sin(theta) * cos(psi))
    out -= 0.5 * cos(2*phi) * sin(2*theta) * sin(psi)
    return out

def Fpydz2(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = math.sqrt(3)/2
    out *= sin(2*theta) * sin(psi)
    return out

def Fpydxy(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = cos(2*phi) * sin(theta) * cos(psi)
    out -= 0.5 * sin(2*phi) * sin(2*theta) *sin(psi)
    return out

def Fpydxz(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = - (sin(phi) * cos(theta) * cos(psi))
    out -= cos(phi) * cos(2*theta) * sin(psi)
    return out

def Fpydyz(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = cos(phi) * cos(theta) * cos(psi)
    out -= sin(phi) * cos(2*theta) * sin(psi)
    return out

#################################### PI X #####################################
def Fpxdx2y2(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = - (sin(2*phi) * sin(theta) * sin(psi))
    out += 0.5 * cos(2*phi) * sin(2*theta) * cos(psi)
    return out

def Fpxdz2(theta,phi,psi=0): #@UnusedVariable
    '''Functional form from Figgis and Hitchman, 2000'''
    out = - math.sqrt(3)/2
    out *= sin(2*theta) * cos(psi)
    return out

def Fpxdxy(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = cos(2*phi) * sin(theta) * sin(psi)
    out += 0.5 * sin(2*phi) * sin(2*theta) *cos(psi)
    return out

def Fpxdxz(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = - (sin(phi) * cos(theta) * sin(psi))
    out += cos(phi) * cos(2*theta) * cos(psi)
    return out

def Fpxdyz(theta,phi,psi=0):
    '''Functional form from Figgis and Hitchman, 2000'''
    out = cos(phi) * cos(theta) * sin(psi)
    out += sin(phi) * cos(2*theta) * cos(psi)
    return out

dict_fsigma = {
    0 : Fsdxy,
    1 : Fsdyz,
    2 : Fsdz2,
    3 : Fsdxz,
    4 : Fsdx2y2
    }
dict_fpix = {
    0 : Fpxdxy,
    1 : Fpxdyz,
    2 : Fpxdz2,
    3 : Fpxdxz,
    4 : Fpxdx2y2
    }
dict_fpiy = {
    0 : Fpydxy,
    1 : Fpydyz,
    2 : Fpydz2,
    3 : Fpydxz,
    4 : Fpydx2y2
    }
dict_overlap_function = {
    'sigma' : dict_fsigma,
    'pix' : dict_fpix,
    'piy' : dict_fpiy
    }

def dMatrixContribution(theta: float, phi: float, psi: float, interaction_type: str) -> np.array:
    """
    Computes a ligand field matrix contribution to the total matrix, as in
    R. J. Deeth, D. L. Foulis, Phys. Chem. Chem. Phys. 2002, 4, 4292-4297.
    Supporting information, equation (47).
    @param theta, phi: Float. Polar coordinates of the ligand.
    @param interaction_type: 'sigma', 'pix', 'piy'. Specifies the overlap function.
    @return: 2D numpy array. Resembles the matrix contribution for one ligand as
        in the given source.
    """
    out = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            out[i][j] = dict_overlap_function[interaction_type][i](theta, phi, psi)\
                        * dict_overlap_function[interaction_type][j](theta, phi, psi)
    return out

def dsFunctions(theta: float, phi: float, psi = 0) -> np.array:
    """
    Computes a d-s mixing vector contribution to the ds-mixing term bb^T as in
    R. J. Deeth, D. L. Foulis, Phys. Chem. Chem. Phys. 2002, 4, 4292-4297.
    Supporting information. Elements as in equation (44) and meaning in eq (45).
    @param theta, phi: Float. Polar coordinates of the ligand.
    @return: 1D numpy array. Resembles the vector b for one ligand as in the given source.
    """
    out = np.zeros((5))
    for i in range(5):
        out[i] = dict_fsigma[i](theta, phi, psi)
    return out

def fifteenEquationsSystemRestrained(es: list[float], coefs: list[list], ligand_field_matrix: list[list[float]], ds_index: int, params_per_ligand: int,
                                     fixed: list[float]=[], fixmap: list[bool]=[], pipairs: list[bool]=[], verbose: bool=False, groups: list[list[int]]=[],
                                     group_scaling_factor: float=1, extra_equations: list[str]=[]):
    """
    Function specific for scipy.optimize.least_squares()
    The equation system resulting from the 5x5 ligand field matrix always has
    15 equations (not necessarily independent), 5 from the diagonal elements
    and 10 from one half of the off-diagonal elements. Since the matrix is always
    symmetric, the other half is not relevant.
    @param es: 1D list of fittable e parameters and energy [E, es1, ep1, eds1, es2, ep2, eds2, ... ]
        E must be the first element to ensure the ds-mixing implementation is working.
    @param coefs: First index is the corresponding e parameter, so this index is the same as in es.
        Second and third dimension are the matrix indices i and j.
        Coefs for ds-mixing are only a 1D sublist with D_i as elements.
    @param ligand_field_matrix: List of one electron ligand field matrices as
        computed by ORCA.
    @param ds_index: Integer. Index of the first ds-mixing parameter and pack
        size at the same time. This index and every ds_index steps after,
        the ds-summation rule is applied.
    @param params_per_ligand: Integer. How many parameters are fitted per ligand. # fitted? fit fit fit? fit fat fitten?
    @param fixed: List of parameters that are not subject to the fitting procedure.
        Requires fixmap to be provided.
    @param fixmap: List of booleans or integers. Length must be the same as
        fixed + es. fixmap determines how the parameters are put together to be
        filled in the equation system.
    @param pipairs: List of booleans or integers. Length must be the same as the
        number of ligands. Sets epix/epiy grouping for the indexed ligands,
        0 is no grouping and 1 is grouping, eg for six ligands: [0,0,1,1,1,1]
    @param verbose: print the equation System.
    @param groups: Atom groups which parameters should be equal. Must be provided
        as indices: [[0,1],[2,3]]
    @param group_scaling_factor: Float. Scaling factor for the grouping equations.
        Determines the weight of ligand grouping.
    @attention: es and coefs must be of the same length!
    @return: 1D list of the summed equation system results for V_ij
        [V_00, V_01, V_02, V_03, V_04, V_10, V_11, V_12, ...]
    """
    # Information: len(coefs) is almost certainly 1 for every case, it is
    # a relic which was too tedious to remove so far
    
    #Create the full parameter list first
    if len(fixed) == 0 or len(fixmap) == 0:
        all_es = es
    else:
        all_es = []
        i,j = 0,0
        for is_fixed in fixmap:
            if is_fixed:
                all_es.append(fixed[i])
                i += 1
            else:
                all_es.append(es[j])
                j += 1
    
    out = list(it.repeat(0, 15)) # 15 equations per file
    position = -1
    if verbose: outstring = list(it.repeat('', 15))
    for i in range(5):
        for j in range(i, 5):
            position += 1
            out[position] -= ligand_field_matrix[i][j]
            if verbose:
                outstring[position] += f'Eq [{position:2}]: {-1 * ligand_field_matrix[i][j]:+11.2f} '
            for x, e in enumerate(all_es):
                if x % ds_index == 0 and x != 0:
                    #skip ds-mixing term here, but not Energy (index 0)
                    continue
                else:
                    out[position] += coefs[x][i][j] * e
                    
                    if verbose: outstring[position] += f'{coefs[x][i][j]:+7.3f} * e[{x}] '
            if ds_index != -1000:
                if verbose:
                    part_i = '( '
                    part_j = '( '
                ds_i, ds_j = 0, 0 # Because ds-mixing must be summed before multiplication
                for s in it.count(ds_index, ds_index):
                    if s >= len(all_es):
                        break
                    else:
                        ds_i += coefs[s][i] * np.sqrt(all_es[s])
                        ds_j += coefs[s][j] * np.sqrt(all_es[s])
                        if verbose:
                            part_i += f'+ {coefs[s][i]:.2f} * \u221Aeds[{s}] ' #\u221A is the root character
                            part_j += f'+ {coefs[s][j]:.2f} * \u221Aeds[{s}] '
                out[position] -= (ds_i * ds_j)
                if verbose:
                    part_i += ') '
                    part_j += ') '
                    outstring[position] += '- ' + part_i + part_j
    # Ligand grouping block
    i = 0
    for group in groups:
        for a,b in it.combinations(group, 2):
            for j in range(params_per_ligand):
                i += 1
                add_to_out = 0
                add_to_out += all_es[(a*params_per_ligand)+1+j]
                add_to_out -= all_es[(b*params_per_ligand)+1+j]
                add_to_out /= binom(len(group),2) #Check if that correction makes sense 
                # -> Checked, yes it makes sense, although weighting is probably
                # not linear because of the square-root system. Anyway, there is
                # so much guessing and eye balling in applying the procedure,
                # this is a small concern.
                add_to_out *= group_scaling_factor
                if verbose:
                    outstring.append((f'Res[{i:2}]: {group_scaling_factor}*(e[{(a*params_per_ligand)+1+j}] - e[{(b*params_per_ligand)+1+j}]) '
                                      f'/ ({binom(len(group),2)})'))
                out.append(add_to_out)
    # epi grouping block
    for i,pipair in enumerate(pipairs):
        if pipair:
            add_to_out = 0
            add_to_out += all_es[i*params_per_ligand+2]
            add_to_out -= all_es[i*params_per_ligand+3]
            add_to_out *= group_scaling_factor
            if verbose:
                outstring.append((f'epi[{i:2}]: {group_scaling_factor}*(e[{i*params_per_ligand+2}] - e[{i*params_per_ligand+3}]) '))
            out.append(add_to_out)
    #extra equations
    for eq in extra_equations:
        out.append(eval(eq))
        if verbose:
            outstring.append(eq)
    if verbose:
        for i in range(len(out)):
            outstring[i] += f'= {out[i]:+.1f}'
        print(*outstring, sep='\n')
    return out
