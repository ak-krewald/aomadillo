"""
Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""
import numpy as np

class Atom():
    """
    Atom class for cartesian coordinates, with an element
    """

    def __init__(self, element, x, y, z):
        self.element = element
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
    
    def __str__(self):
        return f'{self.element:>4s} {self.x: 12.9f} {self.y: 12.9f} {self.z: 12.9f}'
    
    def getCoords(self):
        return np.array([self.x, self.y, self.z])
    
    def setCoords(self, np_array):
        self.x, self.y, self.z = np_array
        return
    
    def getListFormat(self):
        return [self.element, self.x, self.y, self.z]
    
    def move(self, x, y, z):
        self.x += x
        self.y += y
        self.z += z
        return

#EOClass