"""
Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""

class DOrbital():
    """
    Class for storing and providing d orbital data.
    """
    def __init__(self, number, energy, occupation, dxy, dyz, dz2, dxz, dx2y2):
        self.number = int(number)
        self.energy = float(energy)
        self.occupation = float(occupation)
        self.dxy = float(dxy)
        self.dyz = float(dyz)
        self.dz2 = float(dz2)
        self.dxz = float(dxz)
        self.dx2y2 = float(dx2y2)
    
    def __str__(self) -> str:
        return f'{self.number:>3} {self.energy:>8} {self.occupation:>8} | {self.dxy:>5} {self.dyz:>5} {self.dz2:>5} {self.dxz:>5} {self.dx2y2:>5} | {self.getTotalD():>5}'
    
    @staticmethod
    def getHeader() -> str:
        return f'{"No":>3} {"Energy":>8} {"Occ":>8} | {"dxy":>5} {"dyz":>5} {"dz2":>5} {"dxz":>5} {"dx2y2":>5} | {"sum":>5}'
    
    def getTotalD(self) -> float:
        """
        Returns the sum of the d function portions (c squared).
        """
        return sum(self.getDList())
    
    def getDList(self) -> list[float]:
        """
        Returns a five-elements list with the d function portions of each orbital.
        """
        return [self.dxy, self.dyz, self.dz2, self.dxz, self.dx2y2]

    def isADOrbital(self, threshold: float=70) -> bool:
        return (self.getTotalD() >= threshold)

def determineMode(inp: str) -> str:
    """
    Check what type of calculation has been done and return a corresponding str.
    @param inp: String. Name of the file.
    @return: String. Can be one of 'casscf', 'opt' and 'sp'.
    """
    input_part = []
    reading = False
    with open(inp, 'r') as f:
        for line in f:
            if reading: input_part.append(line)
            if 'INPUT FILE' in line and not reading: reading = True
            if '****END OF INPUT****' in line and reading: break
    input_string = ' '.join(input_part)
    input_string = input_string.lower()
    
    if '%casscf' in input_string: return 'casscf'
    elif 'opt' in input_string: return 'opt'
    else:
        #warn('No keyword found, proceeding as if it was an sp-file.')
        return 'sp'

def getFirstElement(file: str) -> str:
    """
    Reads in a geometry from an ORCA output file and returns the element symbol
    of element 0.
    @param file: String. Path to the output file to be read.
    @return: String. Symbol of the first element.
    """
    with open(file, 'r') as f:
        for line in f:
            if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                f.readline()
                return f.readline().split()[0]

def getActiveOrbitals(output_file: str) -> list[DOrbital]:
    """
    @param output_file: ORCA output file from a successful (!) CAS calculation
    @return list of DOrbital objects of the orbitals in the active space
    """
    switch = determineMode(output_file)
    metal = getFirstElement(output_file)
    
    mo_block = []
    writing = False
    
    start_phrase = {
        'opt' : 'LOEWDIN REDUCED ORBITAL POPULATIONS PER MO',
        'sp' : 'LOEWDIN REDUCED ORBITAL POPULATIONS PER MO',
        'casscf' : 'LOEWDIN ORBITAL-COMPOSITIONS'
    }
    
    end_phrase = {
        'opt' : 'SPIN DOWN',
        'sp' : 'SPIN DOWN',
        'casscf' : 'LOEWDIN REDUCED ACTIVE MOs'
    }
    
    block_number = 0
    with open(output_file, 'r') as f:
        for line in f:
            if start_phrase[switch] in line:
                writing = True
                mo_block.append([])
            if end_phrase[switch] in line and writing:
                writing = False
                block_number += 1
            if writing:
                mo_block[block_number].append(line)
    
    for i in range(len(mo_block)):
        mo_block[i] = mo_block[i][4:]
    
    for block in mo_block:
        pos = 0
        numbers = []
        energies = []
        occupations = []
        dxy, dyz, dz2, dxz, dx2y2 = [], [], [], [], []
        for line in block:
            ######### PROCESS HEAD #########
            if pos == 0:
                pos += 1
                numbers += line.split()
                dxyset, dyzset, dz2set, dxzset, dx2y2set = False, False, False, False, False
                continue
            if pos == 1:
                pos += 1
                energies += line.split()
                continue
            if pos == 2:
                pos += 1
                occupations += line.split()
                continue
            if pos == 3:
                pos += 1
                continue
            ######### RESET AT EMPTY LINE ########
            if line == '\n':
                pos = 0
                if not dxyset:
                    dxy += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dyzset:
                    dyz += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dz2set:
                    dz2 += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dxzset:
                    dxz += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dx2y2set:
                    dx2y2 += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                continue
            ######### READ IN ORBITALS ########
            else:
                if f'0 {metal:2} dxy' in line:
                    dxy += line.split()[3:]
                    dxyset = True
                    continue
                if f'0 {metal:2} dyz' in line:
                    dyz += line.split()[3:]
                    dyzset = True
                    continue
                if f'0 {metal:2} dz2' in line:
                    dz2 += line.split()[3:]
                    dz2set = True
                    continue
                if f'0 {metal:2} dxz' in line:
                    dxz += line.split()[3:]
                    dxzset = True
                    continue
                if f'0 {metal:2} dx2y2' in line:
                    dx2y2 += line.split()[3:]
                    dx2y2set = True
                    continue
        
        act_orbs = []
        for table_line in zip(numbers, energies, occupations, dxy, dyz, dz2, dxz, dx2y2):
            try:
                occ = float(table_line[2])
            except ValueError:
                continue
            if occ > 0 and occ < 2:
                act_orbs.append(DOrbital(*table_line))
        
        return act_orbs