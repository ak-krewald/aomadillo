#!/usr/bin/env python

"""
Created on 18 Jan 2021

Reads in an input file and an xyz geometry and creates xyz files with random
angle displacements. Define the type of the displacement in the input file.

Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""

GLOBAL_DESCRIPTION = """
aomadillo_sampler.py - create structural samples from a reference geometry

This tool generates structural samples with a random bond angle displacement
from a reference geometry and an input file with the angle definitions. The
geometry needs to be provided as cartesian coordinates file. The input file
is specified in the AOMadillo manual and an example input file can be found
in the directory 'basis'.
"""

import numpy as np

from basis.atom import Atom
from basis import aom_fitting as af


def readInput(inpu: str, coords: list[Atom]) -> list[dict]:
    """
    Reads input file, which has to be defined via keywords.
    @param coords: 1D List of Atom objects.
    @return: List of dictionaries containing the definitions.
    """
    try:
        with open(inpu, 'r') as inp:
            out = []
            block = {}
            for line in inp:
                line = line.split()
                if len(line) == 0:
                    continue
                elif line[0][0] == '#':
                    continue
                elif line[0] == 'bond':
                    bond_atoms = list(map(int, line[1].split(',')))
                    a = bond_atoms.pop(0)
                    target_atoms = np.array(list(map(lambda x: coords[x].getCoords(), bond_atoms)))
                    target_average = np.average(target_atoms, axis=0)
                    block['fromto'] = target_average - coords[a].getCoords()
                    block['fromto'] /= np.linalg.norm(block['fromto'])
                elif line[0] == 'atoms':
                    block['atoms'] = list(map(int, line[1].split(',')))
                elif line[0] == 'bondrel':
                    bonds = line[1].split(',')
                    block['bonds'] = np.linspace(float(bonds[0]), float(bonds[1]), num=int(bonds[2]))
                elif line[0] == 'angrel':
                    block['angs'] = list(map(float,line[1].split(',')))
                elif line[0] == 'groupend':
                    out.append(block)
                    block = {}
                elif line[0] == 'seed':
                    continue #processed separately
                else:
                    print('Unrecognized line:')
                    print(line)
    except Exception as e:
        print('Exception in the input file:')
        print(e)
        raise e
    return out

def getSeed(inpu: str) -> str:
    """
    Reads input file and looks for a seed.
    """
    with open(inpu, 'r') as inp:
        out = None
        for line in inp:
            line = line.split()
            if len(line) > 0 and line[0] == 'seed':
                out = line[1]
    return out

def checkCompleteness(definitions: dict):
    """
    Checks whether the informations of the input and the coordinates fit. Does
    not return anything, but prints warnings on screen if something seems odd.
    """
    atoms = []
    last_steps = len(definitions[0]['bonds'])
    for defi in definitions:
        if len(defi['bonds']) != last_steps:
            print('ERROR: You are performing a different amount of steps for several groups.')
            print('       This is not possible (yet). Please make sure every bondrel definition')
            print('       uses the same amount of steps.')
            quit(1)
        for atom in defi['atoms']:
            atoms.append(atom)
        #Warnung wenn die Winkel 0 enthalten
        
    atoms.sort()
    for i in range(len(atoms)-1):
        if atoms[i] == atoms[i+1]:
            print('WARNING: There are atoms belonging to two or more groups!')
    atoms = set(atoms)
    return

def readGeometry(xyzfile: str) -> list[Atom]:
    """
    Reads in an xyz-file and returns a list of Atom objects.
    @param xyzfile: Path to an xyz-file to be read. xmol format (ORCA) and plain
        coordinates are allowed.
    @return: List of Atom objects, preserving the sequence of the file.
    """
    with open(xyzfile, 'r') as f:
        full_file = f.readlines()
    
    if len(full_file[0].split()) == 1:
        full_file = full_file[2:]
    
    out = []
    for line in full_file:
        out.append(Atom(*line.split()))
        
    return out
    

if __name__ == '__main__':
    af.check_license()
    import argparse
    import copy
    from scipy.spatial.transform import Rotation
    import random
    
    default_samples = 5
    
    parser = argparse.ArgumentParser(description = GLOBAL_DESCRIPTION, formatter_class = argparse.RawDescriptionHelpFormatter)
    requiredNamed = parser.add_argument_group('required named arguments')
    
    parser.add_argument('input', help = 'input file with group definitions')
    parser.add_argument('reference', help = 'reference (optimised geometry)')
    
    parser.add_argument('-n', '--number', help = f'number of random angle samples per bond length. Default = {default_samples}', type = int, default = default_samples)
    parser.add_argument('-p', '--prefix', help = 'prefix for every output file name.', default = '')
    parser.add_argument('-s', '--suffix', help = 'suffix for every output file name.', default = '')
    
    args = parser.parse_args()
    
    if args.prefix != '':
        args.prefix = args.prefix + '-'
    if args.suffix != '':
        args.suffix = '-' + args.suffix
    
    coords = readGeometry(args.reference)
    print(coords[0].getListFormat())
    central_atom = coords[0].getListFormat()
    for i in range(len(coords)):
        coords[i].move(- float(central_atom[1]), - float(central_atom[2]), - float(central_atom[3]))
    
    definitions = readInput(args.input, coords)
    print(f'Definitions read: {len(definitions)}')
    checkCompleteness(definitions)
    steps = len(definitions[0]['bonds'])
    seed = getSeed(args.input)
    if seed is not None: random.seed(seed)
    
    print(f'Output files: {args.prefix}sample-ii-jj{args.suffix}.xyz')
    for i in range(steps):
        for j in range(args.number):
            current_coords = copy.deepcopy(coords) #@type current_coords: list[Atom]
            
            for d in definitions:
                angles = [random.choice((-1,1))*random.uniform(*d['angs']), random.choice((-1,1))*random.uniform(*d['angs']), random.choice((-1,1))*random.uniform(*d['angs'])]
                rot = Rotation.from_euler(seq='xyz', angles=angles, degrees=True)
                for atom in d['atoms']:
                    current_coords[atom].move(*(d['fromto'] * d['bonds'][i]))
                    current_coords[atom].setCoords(np.matmul(current_coords[atom].getCoords(), rot.as_matrix()))
            with open(f'{args.prefix}sample-{i:02d}-{j:02d}{args.suffix}.xyz', 'w') as o:
                o.write(f'{len(current_coords)}\n')
                o.write(f'sampled for you by: structure_sampler.py\n')
                for coord in current_coords:
                    coord.move(float(central_atom[1]), float(central_atom[2]), float(central_atom[3]))
                    o.write(str(coord) + '\n')
                
                
        
    