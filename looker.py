#!/usr/bin/env python

'''
@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt

"Es ist schön, aber zu nichts nütze!" - Asterix und das Geheimnis des Zaubertranks
'''

import tkinter as tk
from tkinter import filedialog
from basis import aom_fitting as af
from scipy.optimize import least_squares
import threading, time
import queue

dic_interaction_to_number = {
    's' : 0,
    'px' : 1,
    'py' : 2,
    'ds' : 3
    }
dic_number_to_interaction = {
    0 : 's',
    1 : 'px',
    2 : 'py',
    3 : 'ds'
    }



class Spinboxx(tk.Spinbox):
    """
    Modified Spinbox class which enables scrolling to increase/decrease the values.
    Stolen and adapted from
    https://stackoverflow.com/questions/38329996/enable-mouse-wheel-in-spinbox-tk-python
    """
    def __init__(self, *args, **kwargs):
        tk.Spinbox.__init__(self, *args, **kwargs)
        self.bind('<MouseWheel>', self.mouseWheel)
        self.bind('<Button-4>', self.mouseWheel)
        self.bind('<Button-5>', self.mouseWheel)

    def mouseWheel(self, event):
        if event.num == 5 or event.delta < 0:
            self.invoke('buttondown')
        elif event.num == 4 or event.delta > 0:
            self.invoke('buttonup')
            
class ThreadedTask(threading.Thread):
    def __init__(self, gui):
        super().__init__()
        self.event = threading.Event()
        self.daemon = True
        #self.queue = queue
        self.gui = gui
    def run(self):
        while not self.event.is_set():
            start = time.time()
            solution = self.gui.runFit()
            self.gui.showSolution(solution)
            end = time.time()
            remain = 1 - (end - start)
            if remain > 0:
                time.sleep(remain)
            

class GUI():
    labels_ligands = []
    label_cost, label_conv = None, None
    spbox_group = []
    spbox_psis = []
    spbox_energy = None
    spboxs_ligands = [[],[],[],[]]
    bitlst = [[],[],[],[]]
    bit_energy = None
    chbox_energy = None
    chboxs_ligands_s = []
    chboxs_ligands_px = []
    chboxs_ligands_py = []
    chboxs_ligands_ds = []
    ligands = []
    task = None
    
    polar_file = None
    ligand_field_matrix = None
    grouped_coefs = None
    
    fittable, constant = None, None #list of floats, list of floats
    running_solver = False # threading.Thread
    
    
    def __init__(self, master):
        self.master = master
        self.master.title('looker.py')
        self.master.geometry('800x600')
        
        menu = tk.Menu(self.master)
        menu_file = tk.Menu(menu)
        menu_file.add_command(label="Open", command=self.onOpen)
        #menu_file.add_command(label="Save")
        menu_file.add_command(label="Exit", command=self.master.quit)
        menu.add_cascade(label='File', menu=menu_file)
        self.master.config(menu=menu, width=800, height=600)
        
        self.bit_energy = tk.BooleanVar()
        
        entry_atoms = tk.Button(self.master, text='Run fit', command=self.fullRun).grid(column=0, row=0)
        butto_atoms = tk.Button(self.master, text='Continuous run', command=self.tb_click).grid(column=1, row=0)
        
        label_orbs = tk.Label(self.master, text='pic goes here').grid(column=0,row=1)

    def tb_click(self):
    #     self.progress()
    #     self.prog_bar.start()
        if not self.running_solver:
            self.running_solver = True
            #self.queue = queue.Queue()
            self.task = ThreadedTask(self)
            self.task.start()
            #self.master.after(100, self.process_queue)
        else:
            self.running_solver = False
            self.task.event.set()
    
    def process_queue(self):
        try:
            msg = self.queue.get_nowait()
            print(msg)
            # Show result of the task if needed
            # self.prog_bar.stop()
        except queue.Empty:
            self.master.after(100, self.process_queue)
    
    def makeFields(self):
        self.spbox_energy = Spinboxx(master, from_=-10000000, to=0, increment=50, width=9, justify=tk.RIGHT)
        self.spbox_energy.delete(0,"end"); self.spbox_energy.insert(0, '-1000000')
        self.spbox_energy.grid(column=4, row=0)
        self.chbox_energy = tk.Checkbutton(self.master, variable=self.bit_energy)
        self.chbox_energy.grid(column=5, row=0)
        self.label_cost_ = tk.Label(self.master, text='Cost:', justify=tk.RIGHT)
        self.label_cost_.grid(column=6, row=0)
        self.label_cost = tk.Label(self.master, text='0')
        self.label_cost.grid(column=7, row=0)
        self.label_conv = tk.Label(self.master, text='')
        self.label_conv.grid(column=8, row=0, columnspan=6)
        for i,ligand in enumerate(self.ligands):
            label = tk.Label(master, text=ligand).grid(column=1, row=i+2)
            self.labels_ligands.append(label)
            spinbox = Spinboxx(master, from_=-10000, to=30000, increment=50, width=6, justify=tk.RIGHT)
            spinbox.delete(0,"end"); spinbox.insert(0,0)
            spinbox.grid(column=2,row=i+2)
            self.spboxs_ligands[0].append(spinbox)
            spinbox = Spinboxx(master, from_=-10000, to=30000, increment=50, width=6, justify=tk.RIGHT)
            spinbox.delete(0,"end"); spinbox.insert(0,0)
            spinbox.grid(column=4,row=i+2)
            self.spboxs_ligands[1].append(spinbox)
            spinbox = Spinboxx(master, from_=-10000, to=30000, increment=50, width=6, justify=tk.RIGHT)
            spinbox.delete(0,"end"); spinbox.insert(0,0)
            spinbox.grid(column=6,row=i+2)
            self.spboxs_ligands[2].append(spinbox)
            spinbox = Spinboxx(master, from_=-10000, to=30000, increment=50, width=6, justify=tk.RIGHT)
            spinbox.delete(0,"end"); spinbox.insert(0,0)
            spinbox.grid(column=8,row=i+2)
            self.spboxs_ligands[3].append(spinbox)
            
            bitvar = tk.BooleanVar()
            checkbox = tk.Checkbutton(master, variable=bitvar)
            checkbox.grid(column=3, row=i+2)
            self.bitlst[0].append(bitvar)
            self.chboxs_ligands_s.append(checkbox)
            bitvar = tk.BooleanVar()
            checkbox = tk.Checkbutton(master, variable=bitvar)
            checkbox.grid(column=5, row=i+2)
            self.bitlst[1].append(bitvar)
            self.chboxs_ligands_px.append(checkbox)
            bitvar = tk.BooleanVar()
            checkbox = tk.Checkbutton(master, variable=bitvar)
            checkbox.grid(column=7, row=i+2)
            self.bitlst[2].append(bitvar)
            self.chboxs_ligands_py.append(checkbox)
            bitvar = tk.BooleanVar()
            checkbox = tk.Checkbutton(master, variable=bitvar)
            checkbox.grid(column=9, row=i+2)
            self.bitlst[3].append(bitvar)
            self.chboxs_ligands_ds.append(checkbox)
            
            spinbox = Spinboxx(master, from_=0, to=359, increment=1, width=3, justify=tk.RIGHT)
            spinbox.grid(column=10, row=i+2)
            self.spbox_psis.append(spinbox)
            spinbox = Spinboxx(master, from_=0, to=99, increment=1, width=2, justify=tk.RIGHT)
            spinbox.grid(column=11, row=i+2)
            self.spbox_group.append(spinbox)
    
        self.labels_interactions = []
        for i,name in enumerate(['ligand', 'sigma', '', 'pi x', '', 'pi y', '', 'ds', '', 'psi', 'group']):
            label = tk.Label(master, text=name).grid(column=(i)+1, row=1)
            self.labels_interactions.append(label)
        
        return
    
    def printBitlist(self):
        print(self.bit_energy.get(), end=' ')
        for i in range(len(self.ligands)):
            for j in range(4):
                print(self.bitlst[j][i].get(), end=' ')
        print()
        print(self.getParameters())
    
    def getBitlist(self):
        """
        Returns a list that shows which parameter is locked at the moment. Lock
        information is gathered from GUI Checkbox objects. The list contains
        integers, but should be interpreted as list of booleans.
        @return: List of integers. Contains a truth value whether a position is
            fixed (1) or not (0). Sequence matches the list from getParameters.
            Example: [0, 1, 1, 1, 0, 0, ... ]
        """
        out = []
        out.append(self.bit_energy.get())
        for i in range(len(self.ligands)):
            for j in range(4):
                out.append(self.bitlst[j][i].get())
        return out
    
    def getParameters(self):
        """
        Returns a parameter list that is created from the GUI Spinbox objects.
        @return: List of floats. Contains the parameters in the sequence
            [E, esigma0, epix0, epiy0, esigma 1, ... ]
        """
        out = []
        out.append(float(self.spbox_energy.get()))
        for i in range(len(self.ligands)):
            for j in range(4):
                out.append(float(self.spboxs_ligands[j][i].get()))
        return out
    
    
    def runFit(self):
        """
        Gathers parameters from the Spinboxx objects to start the aom fitting
        procedure. Does not write data to any field in the GUI.
        @return: Solution object created by least_squares
        """
        verbose = False
        self.fittable, self.constant = [], []
        lower, upper = [], []
        
        polar_file_psis = []
        psis = list(map(lambda x: float(x.get()), self.spbox_psis))
        for i in range(len(self.polar_file)):
            polar_file_psis.append(self.polar_file[i] + [psis[i]])
        grouped_file = af.groupAtoms(polar_file_psis, list(range(len(self.ligands))), len(self.ligands), dict(zip(list(range(len(self.ligands))), list(range(len(self.ligands))))))
        self.grouped_coefs = af.buildGroupedCoefs(4, grouped_file)
        
        fixmap = self.getBitlist()
        params = self.getParameters()
        low_full, up_full = af.buildBounds(2, 1, len(self.ligands))
        
        for bit, value, low, up in zip(fixmap, params, low_full, up_full):
            if bit:
                self.constant.append(value)
            else:
                self.fittable.append(value)
                lower.append(low)
                upper.append(up)
        
        soft_groups = []
        for group_label in self.spbox_group:
            soft_groups.append(group_label.get())
        soft_groups = af.processSoftGroupList(str(soft_groups))
        
        solution = least_squares(af.fifteenEquationsSystemRestrained,
                                 self.fittable, bounds=(lower, upper),
                                 args=([self.grouped_coefs], [self.ligand_field_matrix], 4, 4),
                                 kwargs={'fixed':self.constant, 'fixmap':fixmap, 'groups':soft_groups})
        
        if verbose:
            af.fifteenEquationsSystemRestrained(solution.x, [self.grouped_coefs], [self.ligand_field_matrix], 4, 4, verbose=True, fixed=self.constant, fixmap=fixmap, groups=soft_groups)
        
        return solution
    
    
    def showSolution(self, solution):
        """
        Processes a Solution object and puts the found solution in the GUI fields.
        @param solution: Solution object created by least_squares.
        """
        fixmap = self.getBitlist()
        cost = solution.cost
        self.label_cost.config(text=str(round(cost,0)))
        convergence = solution.message
        self.label_conv.config(text=str(convergence))
        solution = list(solution.x)
        all_es = []
        for i in fixmap:
            if i:
                all_es.append(self.constant.pop(0))
            else:
                all_es.append(solution.pop(0))
        
        
        self.spbox_energy.delete(0, "end")
        self.spbox_energy.insert(0, str(round(all_es.pop(0), 0)))
        
        for i in range(len(self.ligands)):
            for j in range(4):
                self.spboxs_ligands[j][i].delete(0, "end")
                self.spboxs_ligands[j][i].insert(0, str(all_es.pop(0)))
        master.update()
        return
    
    def fullRun(self):
        """
        Wraps the solver run and solution processing. No parameters, no return.
        """
        solution = self.runFit()
        self.showSolution(solution)
        return
    
    def onOpen(self):
        """
        Handles the actions when the Open Button in the File menu is pressed. Opens
        the file and handles some preprocessing for the solver
        """
        file = filedialog.askopenfilename(initialdir = "~/",title = "Open file",filetypes = (("ORCA output files","*.out"),("All files","*.*")))
        
        xyz_file = af.readGeometry(file)
        xyz_text = af.readGeometry(file, elements=True)
        xyz_text = list(map(lambda x: list(map(lambda y: str(y), x)), xyz_text))
        xyz_text = list(map(lambda x: ' '.join(x), xyz_text))
        
        ligand_window = tk.Toplevel()
        checkboxes = []
        checkbox_vars = []
        labels = []
        var = tk.IntVar()
        for i in range(len(xyz_file)):
            checkbox_var = tk.IntVar()
            checkbox_vars.append(checkbox_var)
            checkbox = tk.Checkbutton(ligand_window, variable=checkbox_var)
            checkbox.grid(column=0, row=i+1)
            checkboxes.append(checkbox)
            label = tk.Label(ligand_window, text=str(xyz_text[i]))
            label.grid(column=1, row=i+1)
            labels.append(label)
        butto_confirm = tk.Button(ligand_window, text='Confirm', command=lambda: var.set(1))
        butto_confirm.grid(column=1, row=0)
        butto_confirm.wait_variable(var)
        
        self.ligands = []
        for i in range(len(xyz_file)):
            if checkbox_vars[i].get() == 1:
                self.ligands.append(i)
                
        ligand_window.destroy()
        
        xyz_file = [xyz_file[index] for index in self.ligands]
        self.polar_file = [af.xyzToPol(*xyz) for xyz in xyz_file]
        
        self.ligand_field_matrix = af.readLigandFieldOneElectronMatrix(file, mode='CASSCF')
        self.ligand_field_matrix *= af.hartree_to['cm-1'] # unit trafo, because Eh is bulky
        
        self.makeFields()
        
        solution = self.runFit()
        self.showSolution(solution)
        master.update()
        
        return
    



################################# GUI CREATION #################################

master = tk.Tk()
gui = GUI(master)
master.mainloop()

