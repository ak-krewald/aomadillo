#!/usr/bin/env python

"""
Face of the aomadillo project.

Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""

GLOBAL_DESCRIPTION = """
aomadillo_solve.py - main script of the AOMadillo package

If you used this software in your scientific work, please cite:
M. Buchhorn, V. Krewald, Journal of Computational Chemistry 2023

Flag descriptions are kept short in this help message. For more detailed
descriptions, please refer to the manual. For examples or reference data,
check our other publications:
M. Buchhorn, V. Krewald, Dalton Transactions 2023, 52, 6685–6692.
M. Buchhorn, R. J. Deeth, V. Krewald, Chemistry - A European Journal 2022, 28,
e202103775.
"""

from scipy.optimize import least_squares

from basis import aom_fitting as af
import itertools as it
import numpy as np
import sys

def printHeadline(unique_ligands: int, epi: int, nods: bool, precision: int = 0, filewidth: int = 20):
    """
    Prints the headline for the parameter table output. No return value.
    @param unique_ligands: Integer. Number of ligands.
    @param epi: Integer. How many pi parameters are there. 0 = no epi. 1 = epi
        without x/y distinction. 2 = epix and epiy printed separately.
    @param nods: Boolean. With/without d-s mixing.
    @param precision: Integer. How many digits after the comma are printed in
        the table. Does affect the width of the fields.
    @param filewidth: Integer. How broad the field for the filename should be.
    @return: Now return value, prints to stdout.
    """
    print(f'{"#File":{filewidth}} {"Cost":>9}',end='')
    params = ['esigma']
    if epi == 0:
        pass #no string since no epi exists
    elif epi == 1:
        params.append('epi')
    elif epi == 2:
        params.append('epix')
        params.append('epiy')
    if not nods:
        params.append('eds')
    print(f' {"E":>{precision +8}}', end='')
    for _ in range(unique_ligands):
        for e in params:
            print(f' {e:>{precision +8}}', end='')
    print()
    return

def processList(stringed_list: str, cast_function) -> list:
    """
    Processes strings as expected in the argparse input, in the format
    [a1,a2,a3,a4]. Returns List objects where every element is casted with
    the given function.
    @param stringed_list: String representation of a list object.
    @param cast_function: Function. This is mapped on every element in the list.
    @return: List with elements of the type the cast function returns.
    """
    out = stringed_list.strip('[]').split(',')
    out = list(map(cast_function, out))
    return out

def processFixmap(fixmap: list[str], monte: bool) -> (list[int], list[float], list[float]):
    """
    Processes the fixmap argument.
    @param fixmap: List of Strings. Elements can be "x" or interpreted as numbers.
    @param monte: Boolean. True if the unfixed values should be randomly guessed
        (within certain borders)
    @return bitlist: List of Integers, every element can be 0 or 1. ) is a position
        with a loose parameter, 1 is a position with a fixed parameter.
    @return loose: List of Floats. These numbers are going to be unfixed
        and have default values.
    @return fixed: List of Floats. These numbers are going to be fixed,
        the values are read from the input.
    """
    very_first_element = True
    bitlist, loose, fixed = [], [], []
    for e in fixmap:
        if e == 'x':
            if very_first_element:
                loose.append(np.random.randint(-1000000,0))
            elif monte:
                loose.append(np.random.randint(0,10000))
            else:
                loose.append(2000)
            bitlist.append(0)
        else:
            fixed.append(float(e))
            bitlist.append(1)
        very_first_element = False
    assert len(loose) + len(fixed) == len(bitlist)
    return bitlist, loose, fixed


def generatePlotFiles(plotgroup: str, prefix: str, atom_types: list[int], interaction_types: list[str], fitting_results: list[list[float]], lengths_per_file: list[list[float]]):
    """
    Generates table data and simple gnuplot files for plotting.
    @param plotgroup: String. List-like string representation of the separate plptgroups,
        if given. E.g. [A,A,B,B,C].
    @param prefix: String. Prefix for all created files.
    @param atom_types: List of Integers. Contains the numbers of the ligand atoms.
    @param interaction_types: List of strings. Each string denotes an interaction type (e.g. sigma, pi, ds).
    @param fitting_results: 2D List. First dimension is the file index, second dimension is
        the parameter as returned by scipy.least_squares.
    @param lengths_per_file: 2D List. First dimension is the file index, second dimension
        is the bond length of the ligand atom at the respective index.
    @return: no return value, writes data to files
    """
    reduced_types = sorted(set(atom_types))
    unique_ligands = len(reduced_types)
    
    if plotgroup is not None:
        plot_atom_types = af.processPlotAtoms(plotgroup)
        plot_reduced_types = list(set(plot_atom_types))
        plot_unique_ligands = len(plot_reduced_types)
        if len(plot_atom_types) != len(atom_types):
            raise ValueError('The number of plot atom groups does not match the number of atoms.')
    else:
        plot_atom_types = atom_types
        plot_reduced_types = reduced_types
        plot_unique_ligands = unique_ligands
        
    with open(f'{prefix}-energy.csv', 'w') as o:
        for result, lengths in zip(fitting_results, lengths_per_file):
            o.write(f'{np.average(lengths):.3f}\t{result[0]:.2f}\n')
    
    for i in range(plot_unique_ligands):
        for j, typ in enumerate(interaction_types):
            with open(f'{prefix}-{plot_reduced_types[i]}-{typ}.csv', 'w') as o: #, open(f'{prefix}-{plot_reduced_types[i]}-{typ}-per-file-avgs.csv', 'w') as avgs: ### Never used functionality
                for result, lengths in zip(fitting_results, lengths_per_file):
                    selector = list(map(lambda x: x == plot_reduced_types[i], plot_atom_types))
                    if plotgroup is None:
                        selector = list(map(lambda x:x == plot_reduced_types[i], plot_reduced_types))
                    pre_avgs = []
                    for x in it.compress(range(len(plot_atom_types)), selector):
                        o.write(f'{lengths[x]:.3f}\t'
                                f'{result[len(interaction_types)*x + j + 1]:.2f}\n')
                        pre_avgs.append(result[len(interaction_types) * x + j + 1])
    return

#===============================================================================
# MAIN ROUTINE
#===============================================================================
if __name__ == '__main__':
    af.check_license()
    import argparse
    
    parser = argparse.ArgumentParser(description = GLOBAL_DESCRIPTION, formatter_class = argparse.RawDescriptionHelpFormatter)
    psigroup = parser.add_mutually_exclusive_group()
    
    parser.add_argument('atoms',
                        help = ('Numbers of the ligating atoms, given as list of '
                                'atom numbers, e.g. [1,2,5,6]. Atom count starts '
                                'at 0. Only ligating atoms are required'))
    parser.add_argument('files',
                        help = ('Arbitrary number of ORCA CASSCF output files.'),
                        nargs='+')

    parser.add_argument('-v', '--verbose',
                        help = ('Print more output.'),
                        action = 'store_true')
    parser.add_argument('--nods',
                        help = ('Remove e_ds from the equation system.'),
                        action = 'store_true')
    parser.add_argument('--noep',
                        help = ('Remove e_pi parameters.'),
                        action = 'store_true')
    psigroup.add_argument('--psirad',
                        help = ('Define on-axis angles psi in radians for every '
                                'ligand. Format: [psi1,psi2,psi3,...].'))
    psigroup.add_argument('--psideg',
                        help = ('Define on-axis angles psi in degree for every '
                                'ligand. Format: [psi1,psi2,psi3,...].'))
    psigroup.add_argument('--psirel',
                        help = ('Define on-axis angles psi by giving another atom '
                                'that defines the molecular plane together with the '
                                'metal and ligand atom. Format: [rel1,rel2,rel3,...]'))
    parser.add_argument('--monte',
                        help = ('Make semi-random start guesses for every parameter.'),
                        action = 'store_true')
    parser.add_argument('-m', '--mode',
                        help = ('Switch between "NEVPT2" and "CASSCF", default is CASSCF. '),
                        choices = ['NEVPT2', 'CASSCF'], default = 'CASSCF')
    parser.add_argument('-g', '--group',
                        help = ('Enable soft grouping, requiring ligands with the '
                                'same label to be equal. Format: [t1,t2,t3,...]'),
                        default='[]')
    parser.add_argument('--epigroup',
                        help = ('Define if epix and epiy for a ligand atom should '
                                'be the same (1) or not (0). Format: [a1,a2,a3,...].'))
    parser.add_argument('-w', '--groupweight',
                        help = ('Scales the grouping functions by W. Default is 1.'),
                        type = float, default = 1)
    parser.add_argument('--equations',
                        help = ('Provide a file with additional equations that '
                                'are added to the total equation system.'))
    parser.add_argument('-x', '--fixmap',
                        help = ('Provide a list of fixed values. Format: [f1,f2,f3,...]. '
                                'Must have as many elements as there are parameters. '
                                'Write x for elements which should not be fixed. '
                                'This is a hard requirement.'))
    parser.add_argument('--noheader',
                        help = ('Do not print the default table headline.'),
                        action = 'store_true')
    parser.add_argument('-p', '--plotfiles',
                        help = ('Generate csv-files with the given name as prefix.'))
    parser.add_argument('--plotgroup',
                        help = ('Create groups for the --plotfiles flag, i.e. ligands '
                                'in the same group have their parameters printed in '
                                'the same csv-files. Requires -p/--plotfiles to be set.'))

    
    args = parser.parse_args()
    np.set_printoptions(precision=4,suppress=True)    
    
    atom_numbers = processList(args.atoms, int)
    unique_ligands = len(atom_numbers)
    
    soft_groups = af.processSoftGroupList(args.group)
    
    #implicit twoep
    if args.psirad != None or args.psideg != None or args.psirel != None:
        args.twoep = True
        if args.noep:
            print(("You provided --noep but set angles for a pix/piy distinction.\n"
                   "Doing the latter has no effect when turning off epi."), file=sys.stderr)
            args.twoep = False
    else:
        args.twoep = False
    
    # AOM parameter indexing
    if args.noep: epi = 0
    elif args.twoep: epi = 2
    else: epi = 1
    if args.nods: ds = 0
    else: ds = 1
    
    params_per_ligand = (1 + epi + ds)
    params = unique_ligands * params_per_ligand
    
    #Build up interaction type list
    interaction_types = af.getInteractionTypes(args.nods, params_per_ligand)
    
    if args.nods: ds_index = -1000
    else: ds_index = params_per_ligand
    
    #Build Psi List
    if args.psirad == None and args.psideg == None and args.psirel == None:
        psis = list(it.repeat(0,len(atom_numbers)))
    elif args.psirad != None:
        psis = processList(args.psirad, float)
    elif args.psideg != None:
        psis = processList(args.psideg, float)
        psis = list(map(lambda x: x/57.2958, psis))
    elif args.psirel != None:
        pass #since this part is dynamic, it is covered in the file processing routine below
    
    #Build pipairs
    if args.twoep and args.epigroup != None:
        pipairs = processList(args.epigroup, int)
    else:
        pipairs = list(it.repeat(0,unique_ligands))
    
    ligand_field_matrix_list = []
    fitting_results = []
    lengths_per_file = []
    outstrings = []
    filewidth = len(max(args.files, key=len))
    
    #Build fixmap and guesses
    if args.fixmap != None:
        fixmap_input = processList(args.fixmap, str)
    else:
        fixmap_input = list(it.repeat('x',(params_per_ligand * unique_ligands)+1))
    fixmap, guess, fixed = processFixmap(fixmap_input, False)
    
    # Reduce bounds from guess
    lower,upper = af.buildBounds(epi, ds, unique_ligands)
    lower = list(it.compress(lower, map(lambda x: 1 if x == 0 else 0, fixmap)))
    upper = list(it.compress(upper, map(lambda x: 1 if x == 0 else 0, fixmap)))
    
    #read equations file
    extra_equations = []
    if args.equations != None:
        with open(args.equations, 'r') as f:
            extra_equations = f.readlines()
            extra_equations = list(map(lambda x: x.replace('e[', 'all_es['), extra_equations))
    
    #===========================================================================
    # PROCESSING PER FILE
    #===========================================================================
    for file in args.files:
        # read in ligand field matrix
        ligand_field_matrix = af.readLigandFieldOneElectronMatrix(file, mode=args.mode)
        ligand_field_matrix *= af.hartree_to['cm-1'] # unit trafo, because Eh is bulky
        # read in geometry
        xyz_file = af.readGeometry(file)
        xyz_file = [[ref-shift for ref,shift in zip(atom,xyz_file[0])] for atom in xyz_file] #Shifts the whole structure such that atom 0 is in the centre
        coords_ligands = [xyz_file[index] for index in atom_numbers]
        ligand_centre = np.average(np.array(coords_ligands),axis=0)
        lengths = list(map(np.linalg.norm, coords_ligands))
        lengths_per_file.append(lengths)
        polars_ligands = [af.xyzToPol(*coords) for coords in coords_ligands]
        if args.psirel != None:
            coords_refatoms = [xyz_file[index] for index in processList(args.psirel, int)]
            psis = af.calcPsis(coords_ligands, coords_refatoms)
        for i in range(len(polars_ligands)):
            polars_ligands[i].append(psis[i])
        # group geometry information in unique ligand associated elements
        coef_matrices = af.buildGroupedCoefs(params_per_ligand, polars_ligands, nods=args.nods)
        #re-guess if monte-argument is true
        if args.monte: guess = processFixmap(fixmap_input, True)[1]
        # A
        # | PREPARATION
        ############################# SOLVER CALL #############################
        solution = least_squares(af.fifteenEquationsSystemRestrained,
                                 guess, bounds=(lower, upper),
                                 args=(coef_matrices, ligand_field_matrix, ds_index, params_per_ligand),
                                 kwargs={'groups':soft_groups, 'pipairs':pipairs, 'fixed':fixed, 'fixmap':fixmap, 'group_scaling_factor':args.groupweight, 'extra_equations':extra_equations})
        #######################################################################
        # | PROCESSING
        # V
        if args.verbose:
            print(solution)
            af.fifteenEquationsSystemRestrained(solution.x, coef_matrices, ligand_field_matrix, ds_index, params_per_ligand, verbose=True, groups=soft_groups, pipairs=pipairs, fixed=fixed, fixmap=fixmap, group_scaling_factor=args.groupweight, extra_equations=extra_equations)
        else:
            outstrings.append(f'{file:{filewidth}} {solution.cost:9.0f} ')
            all_es = []
            i,j = 0,0
            for is_fixed in fixmap:
                if is_fixed:
                    all_es.append(fixed[i])
                    i += 1
                else:
                    all_es.append(solution.x[j])
                    j += 1
            outstrings[-1] += ' '.join([f'{e:8.0f}' for e in all_es])
        if args.plotfiles is not None:
            fitting_results.append(solution.x)
    
    ############################## PRINT  RESULTS ##############################
    if not args.noheader and not args.verbose:
        printHeadline(unique_ligands, epi, args.nods, precision=0, filewidth=filewidth)
    for line in outstrings:
        print(line)
    
    ########################### PLOT FILE GENERATION ###########################
    if args.plotfiles is not None:
        generatePlotFiles(args.plotgroup, args.plotfiles, atom_numbers, interaction_types, fitting_results, lengths_per_file)
                    