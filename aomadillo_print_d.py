#!/usr/bin/env python

"""
usage: aomadillo_print_d.py <orca-output-files>

Extracts all löwdin orbital population blocks from the given file, scans for
d orbitals and prints their number and composition to the terminal.

Copyright (C) 2023  Moritz Buchhorn, Vera Krewald

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

@author: Moritz Buchhorn
@contact: moritz.buchhorn@chemie.tu-darmstadt.de
@contact: vera.krewald@tu-darmstadt.de
@organization: AK Krewald, Theoretische Chemie, TU Darmstadt
"""

GLOBAL_DESCRIPTION = """
aomadillo_print_d.py - print d orbital coefficients from ORCA output

Generates a table view of possible d orbitals for terminal output. A possible
d orbital is every orbital with more than 20% d character, so there can be
many orbitals that are not of interest. In doubt, investigate the output
yourself by another tool.

Notice that for the ORCA calculation the option 'largeprint' must be set for
geometry optimizations and single-point calculations. For CASSCF calculations,
this option is not necessary.
"""

import argparse

from numpy import average, std
from basis import aom_fitting as af

def determineMode(inp: str) -> str:
    """
    Check what type of calculation has been done and return a corresponding str.
    @param inp: String. Name of the file.
    @return: String. Can be one of 'casscf', 'opt' and 'sp'.
    """
    input_part = []
    reading = False
    with open(inp, 'r') as f:
        for line in f:
            if reading: input_part.append(line)
            if 'INPUT FILE' in line and not reading: reading = True
            if '****END OF INPUT****' in line and reading: break
    input_string = ' '.join(input_part)
    input_string = input_string.lower()
    
    if '%casscf' in input_string: return 'casscf'
    elif 'opt' in input_string: return 'opt'
    else:
        #warn('No keyword found, proceeding as if it was an sp-file.')
        return 'sp'

def getFirstElement(file: str) -> str:
    """
    Reads in a geometry from an ORCA output file and returns the element symbol
    of element 0.
    @param file: String. Path to the output file to be read.
    @return: String. Symbol of the first element.
    """
    with open(file, 'r') as f:
        for line in f:
            if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                f.readline()
                return f.readline().split()[0]

def getAvgOccupied(occupations, summed):
    d_orb_portion = []
    for occ,su in zip(occupations, summed):
        try:
            occ = float(occ)
            su = float(su)
        except ValueError:
            continue
        if occ > 0 and occ < 2:
            d_orb_portion.append(su)
    if len(d_orb_portion) == 5:
        return average(d_orb_portion), std(d_orb_portion)
    else:
        return 0,0

################################################################################
################################# MAIN ROUTINE #################################
################################################################################

af.check_license()
parser = argparse.ArgumentParser(description = GLOBAL_DESCRIPTION, formatter_class = argparse.RawDescriptionHelpFormatter)
parser.add_argument('files',
                    help = ('ORCA output files from a CASSCF, SP or Opt '
                            'calculation. SP and Opt need the '
                            'Largeprint keyword to be set.'),
                    nargs = '+')
parser.add_argument('-t', '--threshold',
                    help = ('Threshold of d orbital portion (in percent) for the orbitals '
                            'to be printed. Default is 20.'),
                    type = float, default = 20)
args = parser.parse_args()

start_phrase = {
    'opt' : 'LOEWDIN REDUCED ORBITAL POPULATIONS PER MO',
    'sp' : 'LOEWDIN REDUCED ORBITAL POPULATIONS PER MO',
    'casscf' : 'LOEWDIN ORBITAL-COMPOSITIONS'
}

end_phrase = {
    'opt' : 'SPIN DOWN',
    'sp' : 'SPIN DOWN',
    'casscf' : 'LOEWDIN REDUCED ACTIVE MOs'
}

for file in args.files:
    switch = determineMode(file)
    metal = getFirstElement(file)
    
    mo_block = []
    writing = False
    
    block_number = 0
    with open(file, 'r') as f:
        for line in f:
            if start_phrase[switch] in line:
                writing = True
                mo_block.append([])
            if end_phrase[switch] in line and writing:
                writing = False
                block_number += 1
            if writing:
                mo_block[block_number].append(line)
    
    for i in range(len(mo_block)):
        mo_block[i] = mo_block[i][4:]
    
    for block in mo_block:
        pos = 0
        numbers = []
        energies = []
        occupations = []
        dxy, dyz, dz2, dxz, dx2y2 = [], [], [], [], []
        for line in block:
            ######### PROCESS HEAD #########
            if pos == 0:
                pos += 1
                numbers += line.split()
                dxyset, dyzset, dz2set, dxzset, dx2y2set = False, False, False, False, False
                continue
            if pos == 1:
                pos += 1
                energies += line.split()
                continue
            if pos == 2:
                pos += 1
                occupations += line.split()
                continue
            if pos == 3:
                pos += 1
                continue
            ######### RESET AT EMPTY LINE ########
            if line == '\n':
                pos = 0
                if not dxyset:
                    dxy += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dyzset:
                    dyz += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dz2set:
                    dz2 += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dxzset:
                    dxz += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                if not dx2y2set:
                    dx2y2 += ['0.0', '0.0', '0.0', '0.0', '0.0', '0.0']
                continue
            ######### READ IN ORBITALS ########
            else:
                if f'0 {metal:2} dxy' in line:
                    dxy += line.split()[3:]
                    dxyset = True
                    continue
                if f'0 {metal:2} dyz' in line:
                    dyz += line.split()[3:]
                    dyzset = True
                    continue
                if f'0 {metal:2} dz2' in line:
                    dz2 += line.split()[3:]
                    dz2set = True
                    continue
                if f'0 {metal:2} dxz' in line:
                    dxz += line.split()[3:]
                    dxzset = True
                    continue
                if f'0 {metal:2} dx2y2' in line:
                    dx2y2 += line.split()[3:]
                    dx2y2set = True
                    continue
    
        summed = []
        for c2 in zip(dxy, dyz, dz2, dxz, dx2y2):
            summed.append(round(sum(map(float, c2)),1))
        
        avg_d, std_d = getAvgOccupied(occupations, summed)
        
        homo = numbers[occupations.index('0.00000')-1]
        gapline_set = False
        print(file)
        print(f'HOMO: {homo}')
        headline = '{:>3} {:>8} {:>8} | {:>5} {:>5} {:>5} {:>5} {:>5} | {:>5}'.format('No', 'Energy', 'Occ', 'dxy', 'dyz', 'dz2', 'dxz', 'dx2y2', 'total')
        print(headline)
        print('-' * len(headline))
        for table_line in zip(numbers, energies, occupations, dxy, dyz, dz2, dxz, dx2y2, summed):
            if table_line [2] == '0.00000' and not gapline_set:
                gapline_set = True
                print('-' * len(headline))
            if table_line[-1] > args.threshold:
                print('{:>3} {:>8} {:>8} | {:>5} {:>5} {:>5} {:>5} {:>5} | {:>5}'.format(*table_line))
        print('')
        print(f'Average d contribution: {avg_d:.1f}')
        print(f'Standard deviation of d contribution: {std_d:.1f}')
        print('')
